<?php 
require ('config.php');
?>


<!DOCTYPE HTML>
<html>
    <head>
    <link rel="stylesheet" href="http://cdn.webix.com/edge/webix.css" type="text/css"> 
    <script src="http://cdn.webix.com/edge/webix.js" type="text/javascript"></script>  
    </head>
    <body>
        <script type="text/javascript" charset="utf-8">
 		// <--variables
 		var lW = 300; //size for all label
 		
 		var formSelfEdu = [
		 //{ view:"label", label:"Вид" },
            { view:"select", options:"handler.php?api=getfromdb&type=2", label:"Вид", labelWidth: lW, value:1 },	
            { view:"text", label:"Название индивидуальной программы самообразования ", labelWidth: lW  },	
            { view:"text", label:"Формы и сроки прохождения", labelWidth: lW },	
            { view:"text", label:"Результаты", labelWidth: lW },
            { cols: [	
	            { view:"uploader", label:"Подтверждение", link:"dwnldSE", inputWidth: 200},
	            { view:"list",  id:"dwnldSE",  type:"uploader", autoheight:true, borderless:true }
            ]}	
		];
		
		var formPASheet = [
			{ view:"text", label:"Наименование", labelWidth: lW },	
            { view:"text", label:"Реквизиты",    labelWidth: lW },
            { cols: [	
	            { view:"uploader", label:"Подтверждение", link:"dwnldPAS", inputWidth: 200},
	            { view:"list",  id:"dwnldPAS",  type:"uploader", autoheight:true, borderless:true}
            ]}
		];
		
		var formKnowledgeLanguage = [
			{ view:"select", options:"handler.php?api=getfromdb&type=5", name: "lang",  label:"Иностранный язык", labelWidth: lW, value:1 },	
			{ view:"select", options:"handler.php?api=getfromdb&type=3", name: "level", label:"Уровень владения", labelWidth: lW, value:1 },	
            { cols: [	
	            { view:"uploader", label:"Подтверждение", link:"dwnldPAS", inputWidth: 200},
	            { view:"list",  id:"dwnldPAS",  type:"uploader", name: "files", link:"KnowledgeLanguage", autoheight:true, borderless:true}
            ]},
            { view:"button", value: "Отправить", click: submit}
		];
		//--variables-->
 		
 		//functional
 		function submit(){
			webix.message(JSON.stringify($$("KnowledgeLanguage").getValues(), null, 2));
			var formval = $$("KnowledgeLanguage").getValues();
			
			formval.api  = "submit";
			firmval.type = "KnowledgeLanguage";
			console.log(formval);
			//webix.message($$("KnowledgeLanguage").getValues());
			webix.ajax().get("handler.php", $$("KnowledgeLanguage").getValues()); 	
		}
 		
 		
 		//-functional
 		
 		webix.ui({
		   // width:500,
		   cols:[
		   {rows: [
			    {type:"header", template:"Индивидуальные достижения"},
			    {cols: [
			    	  {view:"accordion",   multi:true,
				    	  rows:[
							{ header:"Самообразование", 
							  body: {
							  	view: "form", elements: formSelfEdu	
							  }
							
							},
							{ header:"Патенты, авторские свидетельства", 
							  body: {
							  	view: "form" , elements: formPASheet	
							  }
							
							}
							]
			    	  },
			    	  
			    ]
			    }
		      
		    ]},
		    {rows: [
				{type:"header", template:"Уровень владения иностранным языком"},
				{cols: [
			    	  {view:"accordion",   multi:true,
				    	  rows:[
							{ header:"Самообразование", 
							  body: {
							  	view: "form", id:"KnowledgeLanguage", elements: formKnowledgeLanguage	
							  }
							}
							]
			    	  },
			    	  
			    ]
			    }	
			]}
				
			
				
			
		    
		    ]
		});
 		
        </script>
    </body>
</html>

