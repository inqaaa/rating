<?php

error_reporting (E_ALL);

$config = require 'config.php';

class MeeApi {
	public $db;
	
	private $tableName = "formstype";
	
	public function __construct ($db) {
		$this->db = $db;
	}
	
	public function router () {
		if (!isset ($_GET['api'])) return $this->error ('Задайте api ID через GET.');
		
		$id = trim ($_GET['api']);
		
		if ($id == "getfromdb") {
			
			$type = $_GET['type'];
			if (!isset($type) or $type=="") 
			    return "Error, type not found";
			else 
				return $this->get_info_from_type($_GET['type']); 
				
		}
		if ($id == "submit") {
			return $this->submitTo1c();
		}
		
		return $this->error ('Неизвестный api ID.');
	}
	
	private function send ($a) {
		header ('Content-Type: application/json; charset=UTF-8', TRUE);
		
		echo json_encode ($a, JSON_UNESCAPED_UNICODE);
	}
	
	private function error ($msg) {
		$this->send (array (
			'error' => $msg
		));
	}
    
    private function get_info_from_type($type) {
        
        $sqlCheck = "SELECT id as id, name as value FROM ".$this->tableName." where type=".$type." ";
        $resultCheck = mysqli_query ($this->db, $sqlCheck);
       
        $resultLanguage = "";
       	while ($row = mysqli_fetch_assoc($resultCheck)) {
			 $resultLanguage .= json_encode($row, JSON_UNESCAPED_UNICODE).",";
		}
       
       	
        return  substr($resultLanguage, 0, -1) ;
    }
    
    private function submitTo1c(){
		error_reporting( E_ERROR ); // Отключаем сообщения 
		// Отключаем кэширование для SOAP. Если этого не сделать, 
		// функции веб-сервисов будут работать некорректно. 
		ini_set("soap.wsdl_cache_enabled", "0" ); 
		$client = new SoapClient("http://217.69.201.114:10000/univer_ryazan2/ws/cdo?wsdl", array('login' => "Exchange", 'password' => "vwDHGYa2bF") ); 
		
			$params["id"] 	= iconv('cp1251','utf-8', $_GET['id']); 
			$params["lang"] = iconv('cp1251','utf-8', $_GET['lang']); 
			$params["level"]= iconv('cp1251','utf-8', $_GET['level']);
			$params["file"]= iconv('cp1251','utf-8', Null);
			
		//$resultSOAPAfter = $client->GetKnowledgeLanguage($params);	 
		
		
		return $resultSOAPAfter;
	}

}

$db = mysqli_connect ($config['db.host'], $config['db.user'], $config['db.pass'], $config['db.name']);

if (isset ($config['db.charset']) && $config['db.charset'] != '') mysqli_set_charset ($db, $config['db.charset']);

$api = new MeeApi ($db);

echo "[".$api->router ()."]";

/*echo '[
	{ "id":1, "value":"Banana"   }, 
	{ "id":2, "value":"Papaya"   }, 
	{ "id":3, "value":"Apple" },
]';*/
mysqli_close ($db);

?>